'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isChecked = isChecked;
exports.isEmail = isEmail;
exports.isAlpha = isAlpha;
exports.isAlphaNumeric = isAlphaNumeric;
exports.isNumeric = isNumeric;
exports.isDecimal = isDecimal;
exports.isInteger = isInteger;
exports.isRequired = isRequired;
exports.isUrl = isUrl;
function isChecked(val) {
  if (!val) {
    return { error: 'isChecked', message: 'Field must be checked' };
  }
}

function isEmail(val) {
  var regex = /(^$|^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$)/;
  if (val && !regex.exec(val)) {
    return { error: 'isEmail', message: 'Not a valid email address' };
  }
}

function isAlpha(val) {
  var regex = /^[a-zA-Z]*$/;
  if (val && !regex.exec(val)) {
    return { error: 'isAlpha', message: 'Not a alpha' };
  }
}

function isAlphaNumeric(val) {
  var regex = /^[a-zA-Z0-9]*$/;
  if (val && !regex.exec(val)) {
    return { error: 'isAlphaNumeric', message: 'Not alpha numeric' };
  }
}

function isNumeric(val) {
  var regex = /^[0-9]*$/;
  if (val && !regex.exec(val)) {
    return { error: 'isNumeric', message: 'Not numeric' };
  }
}

function isDecimal(val) {
  var regex = /^[-]?\d*(\.\d+)?$/;
  if (val && !regex.exec(val)) {
    return { error: 'isDecimal', message: 'Not a decimal' };
  }
}

function isInteger(val) {
  var regex = /^-?[0-9]*$/;
  if (val && !regex.exec(val)) {
    return { error: 'isInteger', message: 'Not an integer' };
  }
}

function isRequired(val) {
  if (!val) {
    return { error: 'isRequired', message: 'Field cannot be empty' };
  }
}

function isUrl(val) {
  var regex = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i;
  if (val && !regex.exec(val)) {
    return { error: 'isUrl', message: 'Not a valid url' };
  }
}