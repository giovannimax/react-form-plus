'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utility = require('./utility');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var StateHelper = function () {
  function StateHelper() {
    var _this = this;

    _classCallCheck(this, StateHelper);

    this.updateKey = (0, _utility.guid)();
    this.fields = {};
    this.onChange = 'onChange';

    this._onChange = function (evt) {
      var m = evt.target.attributes.model || evt.target.attributes.name;
      var model = m.value;
      var newState = {};
      newState[model] = evt.target.value;

      if (evt.target.type === 'checkbox') {
        newState[model] = evt.target.checked;
      }

      // this updates state errors
      newState._errors = _this.validate(model, newState[model]);

      _this.setState(newState);
    };
  }

  _createClass(StateHelper, [{
    key: 'refresh',
    value: function refresh() {
      this.fields = {};
      this.updateKey = (0, _utility.guid)();
      this.setState({ _errors: {} });
    }
  }, {
    key: 'useState',
    value: function useState(state, setState) {
      this._state = state;
      this._setState = setState;
    }
  }, {
    key: 'useContext',
    value: function useContext(store, setState) {
      this._store = store;
      this._setState = setState;
    }
  }, {
    key: 'useValidator',
    value: function useValidator(validator) {
      this.validator = _extends({ _options: {} }, validator);
    }
  }, {
    key: 'state',
    value: function state() {
      if (this._store) {
        return this._store.state;
      }
      return this._state;
    }
  }, {
    key: 'setState',
    value: function setState(newState) {
      // useState
      if (this._state && this._setState) {
        this._setState((0, _utility.mutateState)(this._state, newState));
      }

      // useContext
      if (this._store && this.setState) {
        this._store.dispatch(this._setState(newState));
      }
    }
  }, {
    key: 'model',
    value: function model(_model) {
      var opt = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      var res = {
        'data-update-key': this.updateKey,
        model: _model,
        'name': _model
      };

      var state = this.state();

      // validity
      if (this.validator) {
        var errors = void 0;
        if (state._errors) {
          errors = state._errors;
        }
        if (errors && errors[_model]) {
          Object.assign(res, _extends({}, errors[_model]));
        }
      }

      // event
      res[opt.onChange || this.onChange] = this._onChange;

      // value
      res[opt.value || 'value'] = (0, _utility.pathToValue)(state, _model) || (opt.value === 'checked' ? false : '');

      // register this field
      this.fields[_model] = true;
      return res;
    }
  }, {
    key: 'validate',
    value: function validate(model, value) {
      if (!this.validator) {
        return {};
      }

      var res = {};
      var state = this.state();

      var path = model.replace(/\.[0-9]*\./g, '.');
      var rules = (0, _utility.pathToValue)(this.validator, path);
      var keys = Object.keys(rules || {});
      for (var i = 0; i < keys.length; i++) {
        var k = keys[i];
        var rule = rules[k];
        var test = rule;
        var msg = void 0;
        var err = void 0;
        if ((typeof rule === 'undefined' ? 'undefined' : _typeof(rule)) === 'object') {
          test = rule.test;
          msg = rule.message;
          err = rule.error;
        }
        if (typeof test === 'function') {
          var validity = test(value, { rule: rule, state: state });
          if (validity) {
            Object.assign(res, {
              invalid: 'true',
              error: err || validity.error || msg || validity.message,
              message: msg || validity.message
            });
            break;
          }
        }
      }

      var errors = state._errors || {};
      errors[model] = res;
      if (!res.invalid) {
        delete errors[model];
      }

      return errors;
    }
  }, {
    key: 'validateState',
    value: function validateState() {
      var _this2 = this;

      var state = this.state();

      var errors = {};

      Object.keys(this.fields).forEach(function (k) {
        Object.assign(errors, _this2.validate(k, (0, _utility.pathToValue)(state, k)));
      });

      this.setState({ _errors: errors });
    }
  }]);

  return StateHelper;
}();

exports.default = StateHelper;