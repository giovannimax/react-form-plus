'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.guid = guid;
exports.find = find;
exports.findById = findById;
exports.pathToUpdateObject = pathToUpdateObject;
exports.pathToValue = pathToValue;
exports.mutateState = mutateState;

var _immutabilityHelper = require('immutability-helper');

var _immutabilityHelper2 = _interopRequireDefault(_immutabilityHelper);

var _merge = require('merge');

var _merge2 = _interopRequireDefault(_merge);

var _v = require('uuid/v1');

var _v2 = _interopRequireDefault(_v);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function guid() {
  return (0, _v2.default)();
}

function find(state, func, opt) {
  if (!state) {
    return null;
  }

  if (func(state)) {
    opt.result = opt.result || [];
    opt.result.push({ value: _extends({}, state), path: opt.path.join('.') });
    return state;
  }

  opt = opt || {};
  opt.path = opt.path || [];

  if ((typeof state === 'undefined' ? 'undefined' : _typeof(state)) === 'object') {
    // array
    if (state.length !== undefined) {
      for (var i = 0; i < state.length; i++) {
        opt.path.push(i);
        var res = find(state[i], func, opt);
        if (res) {
          if (!opt.all) {
            return res;
          }
        }
        opt.path.pop();
      }
      return null;
    }

    // object
    var keys = Object.keys(state);
    for (var _i = 0; _i < keys.length; _i++) {
      opt.path.push(keys[_i]);
      var _res = find(state[keys[_i]], func, opt);
      if (_res) {
        if (!opt.all) {
          return _res;
        }
      }
      opt.path.pop();
    }
  }

  return null;
}

function findById(state, id, opt) {
  return find(state, function (state) {
    return state.uuid === id;
  }, opt);
}

function pathToUpdateObject(path, value) {
  var p = path.split('.');
  var obj = {};
  var node = obj;

  var _n = null;
  var _node = null;

  p.forEach(function (n, idx) {
    node[n] = {};

    if (idx === p.length - 1) {
      if (value && (typeof value === 'undefined' ? 'undefined' : _typeof(value)) === 'object' && value.$splice !== undefined) {
        _node[_n] = { $splice: [[n, value.$splice]] };
      } else if (value && (typeof value === 'undefined' ? 'undefined' : _typeof(value)) === 'object' && value.$push !== undefined) {
        node[n] = { $push: [value.$push] };
      } else {
        node[n] = { $set: value };
      }
    }

    // keep track for array manipulation
    _n = n;
    _node = node;

    node = node[n];
  });
  return obj;
}

function pathToValue(state, path) {
  var p = path.split('.');
  var node = state;

  for (var i = 0; i < p.length; i++) {
    var n = p[i];
    var v = node[n];
    if (i === p.length - 1) {
      return v;
    }
    if ((typeof v === 'undefined' ? 'undefined' : _typeof(v)) !== 'object') {
      break;
    }
    node = v;
  }
}

function mutateState(state, params) {
  var ih = {};
  Object.keys(params).forEach(function (k) {
    ih = _merge2.default.recursive(ih, pathToUpdateObject(k, params[k]));
  });
  return (0, _immutabilityHelper2.default)(state, ih);
}