'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isInteger = exports.isRequired = exports.isEmail = exports.isChecked = exports.isDecimal = exports.isNumeric = exports.isAlphaNumeric = exports.isAlpha = exports.pathToValue = exports.pathToUpdateObject = exports.findById = exports.find = exports.guid = exports.StateHelper = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _validators = require('./validators');

var _utility = require('./utility');

var _stateHelper = require('./stateHelper');

var _stateHelper2 = _interopRequireDefault(_stateHelper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Fancy = function (_React$Component) {
  _inherits(Fancy, _React$Component);

  function Fancy() {
    _classCallCheck(this, Fancy);

    return _possibleConstructorReturn(this, (Fancy.__proto__ || Object.getPrototypeOf(Fancy)).apply(this, arguments));
  }

  _createClass(Fancy, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        null,
        'Some fancy component.'
      );
    }
  }]);

  return Fancy;
}(_react2.default.Component);

exports.default = Fancy;
exports.StateHelper = _stateHelper2.default;
exports.guid = _utility.guid;
exports.find = _utility.find;
exports.findById = _utility.findById;
exports.pathToUpdateObject = _utility.pathToUpdateObject;
exports.pathToValue = _utility.pathToValue;
exports.isAlpha = _validators.isAlpha;
exports.isAlphaNumeric = _validators.isAlphaNumeric;
exports.isNumeric = _validators.isNumeric;
exports.isDecimal = _validators.isDecimal;
exports.isChecked = _validators.isChecked;
exports.isEmail = _validators.isEmail;
exports.isRequired = _validators.isRequired;
exports.isInteger = _validators.isInteger;