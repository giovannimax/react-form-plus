import React from 'react';

import {
  isAlpha,
  isAlphaNumeric,
  isNumeric,
  isDecimal,
  isChecked,
  isEmail,
  isRequired,
  isInteger
} from './validators';

import {
  guid,
  find,
  findById,
  pathToUpdateObject,
  pathToValue
} from './utility';

import StateHelper from './stateHelper';

export default class Fancy extends React.Component {
  render() {
    return <div>Some fancy component.</div>;
  }
}

export {
  StateHelper,
  // utilities
  guid,
  find,
  findById,
  pathToUpdateObject,
  pathToValue,
  // validators
  isAlpha,
  isAlphaNumeric,
  isNumeric,
  isDecimal,
  isChecked,
  isEmail,
  isRequired,
  isInteger
};
